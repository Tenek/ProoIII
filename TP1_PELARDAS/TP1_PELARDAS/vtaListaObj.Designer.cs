﻿namespace TP1_PELARDAS
{
    partial class vtaListaObj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(vtaListaObj));
            this.lstObjIzq = new System.Windows.Forms.ListBox();
            this.lstObjDer = new System.Windows.Forms.ListBox();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.txtCarga = new System.Windows.Forms.TextBox();
            this.lblCarga = new System.Windows.Forms.Label();
            this.btnCarga = new System.Windows.Forms.Button();
            this.btnTrasladarTodos = new System.Windows.Forms.Button();
            this.btnTrasladarIzquierda = new System.Windows.Forms.Button();
            this.btnTrasladarDerecha = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnAyuda = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstObjIzq
            // 
            this.lstObjIzq.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.lstObjIzq.FormattingEnabled = true;
            this.lstObjIzq.ItemHeight = 20;
            this.lstObjIzq.Location = new System.Drawing.Point(70, 50);
            this.lstObjIzq.Name = "lstObjIzq";
            this.lstObjIzq.Size = new System.Drawing.Size(211, 384);
            this.lstObjIzq.TabIndex = 0;
            // 
            // lstObjDer
            // 
            this.lstObjDer.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.lstObjDer.FormattingEnabled = true;
            this.lstObjDer.ItemHeight = 20;
            this.lstObjDer.Location = new System.Drawing.Point(501, 50);
            this.lstObjDer.Name = "lstObjDer";
            this.lstObjDer.Size = new System.Drawing.Size(211, 384);
            this.lstObjDer.TabIndex = 1;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(282, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(217, 35);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "LISTA DE OBJETOS";
            // 
            // txtCarga
            // 
            this.txtCarga.Location = new System.Drawing.Point(302, 84);
            this.txtCarga.Name = "txtCarga";
            this.txtCarga.Size = new System.Drawing.Size(180, 26);
            this.txtCarga.TabIndex = 3;
            // 
            // lblCarga
            // 
            this.lblCarga.AutoSize = true;
            this.lblCarga.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarga.Location = new System.Drawing.Point(344, 50);
            this.lblCarga.Name = "lblCarga";
            this.lblCarga.Size = new System.Drawing.Size(106, 21);
            this.lblCarga.TabIndex = 4;
            this.lblCarga.Text = "Cargar Objeto";
            // 
            // btnCarga
            // 
            this.btnCarga.Font = new System.Drawing.Font("Engravers MT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCarga.Location = new System.Drawing.Point(369, 116);
            this.btnCarga.Name = "btnCarga";
            this.btnCarga.Size = new System.Drawing.Size(48, 41);
            this.btnCarga.TabIndex = 5;
            this.btnCarga.Text = "C";
            this.btnCarga.UseVisualStyleBackColor = true;
            this.btnCarga.Click += new System.EventHandler(this.btnCarga_Click);
            // 
            // btnTrasladarTodos
            // 
            this.btnTrasladarTodos.Font = new System.Drawing.Font("Engravers MT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrasladarTodos.Location = new System.Drawing.Point(348, 227);
            this.btnTrasladarTodos.Name = "btnTrasladarTodos";
            this.btnTrasladarTodos.Size = new System.Drawing.Size(88, 41);
            this.btnTrasladarTodos.TabIndex = 6;
            this.btnTrasladarTodos.Text = ">>";
            this.btnTrasladarTodos.UseVisualStyleBackColor = true;
            this.btnTrasladarTodos.Click += new System.EventHandler(this.btnTrasladarTodos_Click);
            // 
            // btnTrasladarIzquierda
            // 
            this.btnTrasladarIzquierda.Font = new System.Drawing.Font("Engravers MT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrasladarIzquierda.Location = new System.Drawing.Point(369, 274);
            this.btnTrasladarIzquierda.Name = "btnTrasladarIzquierda";
            this.btnTrasladarIzquierda.Size = new System.Drawing.Size(48, 41);
            this.btnTrasladarIzquierda.TabIndex = 7;
            this.btnTrasladarIzquierda.Text = "<";
            this.btnTrasladarIzquierda.UseVisualStyleBackColor = true;
            this.btnTrasladarIzquierda.Click += new System.EventHandler(this.btnTrasladarIzquierda_Click);
            // 
            // btnTrasladarDerecha
            // 
            this.btnTrasladarDerecha.Font = new System.Drawing.Font("Engravers MT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrasladarDerecha.Location = new System.Drawing.Point(369, 180);
            this.btnTrasladarDerecha.Name = "btnTrasladarDerecha";
            this.btnTrasladarDerecha.Size = new System.Drawing.Size(48, 41);
            this.btnTrasladarDerecha.TabIndex = 8;
            this.btnTrasladarDerecha.Text = ">";
            this.btnTrasladarDerecha.UseVisualStyleBackColor = true;
            this.btnTrasladarDerecha.Click += new System.EventHandler(this.btnTrasladarDerecha_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.Font = new System.Drawing.Font("Engravers MT", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrar.Location = new System.Drawing.Point(359, 321);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(67, 41);
            this.btnBorrar.TabIndex = 9;
            this.btnBorrar.Text = "...";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // btnAyuda
            // 
            this.btnAyuda.Font = new System.Drawing.Font("Engravers MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAyuda.Location = new System.Drawing.Point(540, 9);
            this.btnAyuda.Name = "btnAyuda";
            this.btnAyuda.Size = new System.Drawing.Size(38, 35);
            this.btnAyuda.TabIndex = 10;
            this.btnAyuda.Text = "?";
            this.btnAyuda.UseVisualStyleBackColor = true;
            this.btnAyuda.Click += new System.EventHandler(this.btnAyuda_Click);
            // 
            // vtaListaObj
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.BurlyWood;
            this.ClientSize = new System.Drawing.Size(778, 544);
            this.Controls.Add(this.btnAyuda);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnTrasladarDerecha);
            this.Controls.Add(this.btnTrasladarIzquierda);
            this.Controls.Add(this.btnTrasladarTodos);
            this.Controls.Add(this.btnCarga);
            this.Controls.Add(this.lblCarga);
            this.Controls.Add(this.txtCarga);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lstObjDer);
            this.Controls.Add(this.lstObjIzq);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "vtaListaObj";
            this.Text = "Listado de Objetos";
            this.Load += new System.EventHandler(this.ListaObj_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstObjIzq;
        private System.Windows.Forms.ListBox lstObjDer;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.TextBox txtCarga;
        private System.Windows.Forms.Label lblCarga;
        private System.Windows.Forms.Button btnCarga;
        private System.Windows.Forms.Button btnTrasladarTodos;
        private System.Windows.Forms.Button btnTrasladarIzquierda;
        private System.Windows.Forms.Button btnTrasladarDerecha;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnAyuda;
    }
}