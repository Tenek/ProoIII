﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP1_PELARDAS
{
    public partial class vtaListaObj : Form
    {
        public vtaListaObj()
        {
            InitializeComponent();
        }

        private void ListaObj_Load(object sender, EventArgs e)
        {
            MessageBox.Show(" Bienvenido a la interfaz para cargar la lista de objetos! " + Environment.NewLine +
                Environment.NewLine + " Cualquier duda que tengas presiona el boton de ayuda (?) ");
;        }

        private void btnCarga_Click(object sender, EventArgs e)
        {
            //VALIDACIONES PARA CARGAR NUEVO OBJETO
            //SI EL CAMPO ESTA VACIO
            if (string.IsNullOrEmpty(txtCarga.Text.Trim()))
            {
                MessageBox.Show(" Debe completar el campo de carga para enviar un item a la lista. ");
                return;
            }
            //SI EL OBJ YA EXISTE EN LA LISTA
            if (SiExisteEn(txtCarga.Text, lstObjIzq))
            {
                MessageBox.Show(" Ese objeto ya existe en la lista. Debes borrarlo primero" +
                " para continuar con la transferencia. ");
                return;
            }
            //SI EL OBJETO TIENE NUMEROS O CARACTERES ESPECIALES
            foreach (char c in txtCarga.Text)
            {
                if (!Char.IsLetter(c) && !Char.IsSeparator(c))
                {
                    MessageBox.Show(" El nombre del objeto no puede contener numeros o caracteres especiales. ");
                    return;
                }
            }

            // CARGA DEL OBJETO EN LA LISTA IZQUIERDA
            lstObjIzq.Items.Add(txtCarga.Text.Trim());
        }

        private void btnTrasladarDerecha_Click(object sender, EventArgs e)
        {
            //VALIDACIONES
            //ESTA SELECCIONADO EL OBJETO
            if (EstaVacio(lstObjIzq.SelectedItem)) return;
            //EXISTE EN LA OTRA TABLA
            if (SiExisteEn(lstObjIzq.SelectedItem, lstObjDer))
            {
                MessageBox.Show(" Ese objeto ya existe en la lista. Debes borrarlo primero" +
                " para continuar con la transferencia. ");
                return;
            }
            //ENVIA
            EnviarSelec(lstObjDer, lstObjIzq);

        }

        private void btnTrasladarIzquierda_Click(object sender, EventArgs e)
        {
            //VALIDACIONES
            //ESTA SELECCIONADO EL OBJETO
            if (EstaVacio(lstObjDer.SelectedItem)) return;
            //EXISTE EN LA OTRA TABLA
            if (SiExisteEn(lstObjDer.SelectedItem, lstObjIzq))
            {
                MessageBox.Show(" Ese objeto ya existe en la lista. Debes borrarlo primero" +
                " para continuar con la transferencia. ");
                return;
            }
            //ENVIA
            EnviarSelec(lstObjIzq, lstObjDer);
        }

        private void btnTrasladarTodos_Click(object sender, EventArgs e)
        {
            //VALIDAR SI LA LISTA ESTA VACIA
            if (lstObjIzq.Items.Count == 0)
            {
                MessageBox.Show("La lista izquierda esta vacia.");
                return;
            }
            //ENVIA LOS ITEMS QUE NO ESTAN EN LA DERECHA
            foreach (object item in lstObjIzq.Items)
            {
                if (!SiExisteEn(item, lstObjDer))
                {
                    lstObjDer.Items.Add(item.ToString());
                }
            }
            //LIMPIA LA IZQUIERDA
            lstObjIzq.Items.Clear();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            //VERIFICA SI ESTA VACIO
            if (EstaVacio(lstObjDer.SelectedItem)) return;
            //BORRA EL ITEM
            lstObjDer.Items.Remove(lstObjDer.SelectedItem);
        }

        private bool EstaVacio(object item) //SI ES ESTA SELECCIONANDO ALGO
        {
            if (item == null)
            {
                MessageBox.Show(" No se esta seleccionando nada ");
                return true;
            }
            return false;
        }
        private bool SiExisteEn(object item, ListBox lista) //SI EL ITEM EXISTE EN LA OTRA LISTA
        {
            foreach (object i in lista.Items)
            {
                string a = i.ToString().ToUpper().Trim();
                string b = item.ToString().ToUpper().Trim();
                if (a == b)
                {
                    return true;
                }
            }
            return false;
        }
        private void EnviarSelec(ListBox receptora, ListBox emisora) //ENVIA UN ITEM SELECCIONADO DE UNA LISTA A OTRA
        {
            receptora.Items.Add(emisora.SelectedItem);
            emisora.Items.Remove(emisora.SelectedItem);
            return;
        }

        private void btnAyuda_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bienvenido al menu de ayuda:" + Environment.NewLine + Environment.NewLine +
                "En la interfaz actual podras cargar objetos en la lista izquierda para luego enviarlos mediante los botones a la derecha." + Environment.NewLine +
                "   Botones: " + Environment.NewLine +
                "     C : Este boton sirve para cargar el objeto nombrado en el campo de texto." + Environment.NewLine +
                "     > : Este boton sirve para enviar el objeto seleccionado a la lista derecha " + Environment.NewLine +
                "     < : Este boton es para enviar el objeto seleccionado a la lista de la izquierda" + Environment.NewLine +
                "    >> : Este boton sirve para enviar todos los objetos de la izquierda a la derecha " + Environment.NewLine +
                "   ... : Este boton sirve para borrar un objeto de la lista de la derecha ");

        }
    }
}
