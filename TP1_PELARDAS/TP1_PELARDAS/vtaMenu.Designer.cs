﻿namespace TP1_PELARDAS
{
    partial class vtaMenu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(vtaMenu));
            this.btnCargarPersona = new System.Windows.Forms.Button();
            this.btnListarObj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCargarPersona
            // 
            resources.ApplyResources(this.btnCargarPersona, "btnCargarPersona");
            this.btnCargarPersona.Name = "btnCargarPersona";
            this.btnCargarPersona.UseVisualStyleBackColor = true;
            this.btnCargarPersona.Click += new System.EventHandler(this.btnCargarPersona_Click);
            // 
            // btnListarObj
            // 
            resources.ApplyResources(this.btnListarObj, "btnListarObj");
            this.btnListarObj.Name = "btnListarObj";
            this.btnListarObj.UseVisualStyleBackColor = true;
            this.btnListarObj.Click += new System.EventHandler(this.btnListarObj_Click);
            // 
            // vtaMenu
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.BurlyWood;
            this.Controls.Add(this.btnListarObj);
            this.Controls.Add(this.btnCargarPersona);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "vtaMenu";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCargarPersona;
        private System.Windows.Forms.Button btnListarObj;
    }
}

