﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP1_PELARDAS
{
    public partial class vtaMenu : Form
    {
        public vtaMenu()
        {
            InitializeComponent();
        }

        private void btnCargarPersona_Click(object sender, EventArgs e)
        {
            vtaPersonas persona = new vtaPersonas();
            persona.Show();
        }

        private void btnListarObj_Click(object sender, EventArgs e)
        {
            vtaListaObj listaobj = new vtaListaObj();
            listaobj.Show();
        }
    }
}
