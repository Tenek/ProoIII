﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP1_PELARDAS
{
    public partial class vtaPersonas : Form
    {
        public vtaPersonas() //Iniciar componente y crear los tooltips en las labels.
        {
            InitializeComponent();
            var tt = new ToolTip();
            tt.SetToolTip(lblNombre, "Escribi tu nombre comenzando con mayuscula");
            tt.SetToolTip(lblApellido, "Escibi tu apellido comenzando con mayuscula");
            tt.SetToolTip(lblColores, "Selecciona del menu desplegable tu color preferido");
            tt.SetToolTip(lblFN, "Selecciona tu fecha de nacimiento");
            tt.SetToolTip(lblMusica, "Tilda tus generos musicales preferidos");
            tt.SetToolTip(lblGenero, "Selecciona tu genero");
            tt.SetToolTip(lblTítulo, "Rellena los campos y presiona Aceptar");
        }

        private void vtaPersonas_Load(object sender, EventArgs e) //Mensaje cuando se abre el form
        {
            MessageBox.Show("Llena el siguiente formulario y presiona Aceptar para registrar los datos." 
                + Environment.NewLine + Environment.NewLine 
                + "Pasando con el cursor por arriba de los titulos te ayuda con lo que tengas que rellenar.");
            cbColores.SelectedIndex = 0; //Establece el indice del combobox a 0 para seleccionar un color
                                         //por defecto
        }

        private void btnAceptar_Click(object sender, EventArgs e) //Presionar botón aceptar
        {
            if(!validarFormulario())
            {
                var genero = "";
                if (rbtnFemenino.Checked) genero = "Femenino";
                else genero = "Masculino";
                var ge = generosElegidos();
                //mostrar resumen
                MessageBox.Show("¡Registro cargado exitosamente!" + Environment.NewLine + Environment.NewLine +
                                "Datos Ingresados:" + Environment.NewLine +
                                "   Nombre: " + txtNombre.Text.Trim() + Environment.NewLine +
                                "   Apellido: " + txtApellido.Text.Trim() + Environment.NewLine +
                                "   Genero: " + genero + Environment.NewLine +
                                "   Fecha de Nacimiento: " + dtpFn.Value.Date.ToString("dd/MM/yyyy") + Environment.NewLine +
                                "   Generos Musicales: " + ge + Environment.NewLine +
                                "   Color Preferido: " + cbColores.SelectedItem.ToString());
                this.Close();
            }
        }

        private bool validarFormulario() //Validaciones de los campos, false si están bien, verdadero si hay alguno mal.
        {
            var f = false;
            var c = 0;
            //QUE NO ESTEN LOS CAMPOS VACIOS
            if (string.IsNullOrEmpty(txtNombre.Text.Trim()) || string.IsNullOrEmpty(txtApellido.Text.Trim())) f = true;
            //VALIDACIONES NOMBRE
            foreach (char i in txtNombre.Text.Trim())
            {
                var b = false;
                if (!Char.IsLetter(i) && !Char.IsSeparator(i)) 
                {
                    b = true;
                    txtNombre.ForeColor = Color.Red;
                } else txtNombre.ForeColor = Color.Black;
                c++;
                if (b)
                {
                    f = true;
                    break;
                }
            }

            //VALIDACIONES APELLIDO
            c = 0;

            foreach (char i in txtApellido.Text)
            {
                var b = false;
                if (!Char.IsLetter(i) && !Char.IsSeparator(i))
                {
                    b = true;
                    txtApellido.ForeColor = Color.Red;
                }
                else txtApellido.ForeColor = Color.Black;
                c++;
                if (b)
                {
                    f = true;
                    break;
                }

            }
            //ULTIMAS VALIDACIONES CAMPOS DE TEXTO
            if (txtApellido.Text == txtNombre.Text)
            {
                f = true;
            }

            //VALIDACIONES GENERO
            if ((!rbtnFemenino.Checked) && (!rbtnMasculino.Checked)) f = true;
            //VALIDACIONES MUSICA
            if (clistMusica.CheckedItems.Count == 0) f = true;

            //MENSAJE DE ERROR
            if (f)
            {
                MessageBox.Show("Corregi los campos destacados." + Environment.NewLine + Environment.NewLine +
                                "Errores comunes:" + Environment.NewLine +
                                "-Ningun campo puede estar vacio" + Environment.NewLine +
                                "-Apellido/Nombre debe comenzar con mayuscula" + Environment.NewLine +
                                "-Apellido/Nombre no pueden contener numeros ni ser identicos" + Environment.NewLine +
                                "-Falta seleccionar genero / generos musicales / color / fecha" + Environment.NewLine +
                                "-Fecha Invalida" + Environment.NewLine);
            }
            return f;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            var dias = DateTime.Now.Date.Subtract(dtpFn.Value.Date).Days;
            var años = dias / 365;
            lblEdad.Text = "Usted tiene " + años + " años";
        }

        private string generosElegidos()
        {
            var ge = "";

            foreach (string item in clistMusica.CheckedItems)
            {
                ge = ge + item.ToString() + ", ";
            }

            return ge;
        }

    }
}
