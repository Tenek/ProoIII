﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP2_Pelardas.Clases
{
    public class Cliente : Persona
    {
        public int deuda { get; set; }
        public string categoria { get; set; } //Monotributo, consumidor final o Responsable Inscripto
        public char cateMonotributo { get; set; }
        public string documento { get; set; } //Dni / Cuit
        public string listaPrecio { get; set; } // LIsta 1, Lista 2 o Lista Especial
        public List<Pedido> listaPedidos { get; set; } //coleccion de pedidos
        public List<Factura> listaFacturas { get; set; }//coleccion de facturas

    }
}


