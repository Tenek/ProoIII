﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Compartimiento
    {
        public int id { get; set; }
        public Sector ubicacion { get; set; }
        public Cliente dueñoCompartimiento { get; set; }
    }
}
