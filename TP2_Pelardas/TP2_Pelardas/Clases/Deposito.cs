﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Deposito
    {
        public int id { get; set; }
        public int nombre { get; set; }
        public Direccion direccion { get; set; }
        public int cantPasillos { get; set; }
        public int cantEmpleados { get; set; }
        public string horario { get; set; }

    }
}
