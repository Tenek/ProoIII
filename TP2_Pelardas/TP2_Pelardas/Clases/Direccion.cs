using System;
using System.Collections.Generic;
using System.Text;

namespace TP2_Pelardas.Clases
{
    public class Direccion
    {
        public int numeroPostal { get; set; }
        public string calle { get; set; }
        public string localidad { get; set; }
        public string provincia { get; set; }
    }
}