﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP2_Pelardas.Clases
{
    class Empleado : Persona
    {
        public Deposito deposito { get; set; }
        public DateTime fechaIngreso { get; set; }
        public int sueldo { get; set; }
        public List<Vales> valesMensuales { get; set; }
    }
}
