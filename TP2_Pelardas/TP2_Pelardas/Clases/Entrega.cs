﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Entrega
    {
        public DateTime fechaEntrega { get; set; }
        public Proveedor proveedor { get; set; }
        public Deposito depositoEntrega { get; set; }
        public List<Producto> productos { get; set; }
        public int totalAPagar { get; set; }
    }
}
