﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Factura
    {
        public DateTime fechaFact { get; set; }
        public string nroFactura { get; set; }
        public Cliente cliente { get; set; }
        public string medioPago { get; set; } //efectivo / cuenta corriente / tarjeta / banco
        public List<Producto> productos { get; set; }
        public Pedido pedidoAFacturar { get; set; }
        public int totalACobrar { get; set; }
    }
}
