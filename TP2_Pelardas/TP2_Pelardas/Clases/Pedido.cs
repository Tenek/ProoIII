﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Pedido
    {
        public Cliente cliente { get; set; }
        public DateTime fechaEmision { get; set; }
        public DateTime fechaEntrega { get; set; }
        public Deposito depositoRetiro { get; set; }
        public List<Producto> productos { get; set; }
        public int totalACobrar { get; set; }
    }
}
