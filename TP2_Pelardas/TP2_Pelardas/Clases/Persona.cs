﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP2_Pelardas.Clases
{
    public class Persona
    {
        public int id { get; set; }
        public string nombreApellido { get; set; }
        public Direccion direccion { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public bool estado { get; set; }
    }
}
