﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Producto
    {
        public int id { get; set; }
        public string nombre { get; set; } 
        public Precio precio { get; set; }
        public int cantidad { get; set; }
        public List<Proveedor> proveedor { get; set; } //PROVEEDORES DEL PRODUCTO
        public Compartimiento lugar { get; set; }
    }
}
