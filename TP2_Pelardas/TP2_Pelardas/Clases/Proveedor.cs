﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP2_Pelardas.Clases
{
    public class Proveedor : Persona
    {
        public int saldoFavor { get; set; }
        public int saldoEnContra { get; set; }
        public List<Entrega> listaEntregas { get; set; } //LISTA DE REPARTOS
    }
}
