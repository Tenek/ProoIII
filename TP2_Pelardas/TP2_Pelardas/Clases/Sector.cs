﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Sector
    {
        public Deposito deposito { get; set; }
        public int pasillo { get; set; }
        public string lado { get; set; }
        public Cliente dueñoDelSector { get; set; }
    }
}
