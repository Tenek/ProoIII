﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2_Pelardas.Clases
{
    public class Vales
    {
        public int valor { get; set; }
        public int idEmpleado { get; set; }
        public DateTime fechaEntrega { get; set; }
    }
}
